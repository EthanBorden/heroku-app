// web.js
var redis = require('redis');
var url = require('url');
var redisURL = url.parse(process.env.REDISCLOUD_URL);
var client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
client.auth(redisURL.auth.split(":")[1]);


var express = require("express");
var logfmt = require("logfmt");
var app = express();

app.use(logfmt.requestLogger());

app.get('/', function(req, res) {
  res.send('Welcome to our condiments selction. Choose Wiseley.');
});

app.post('/api/v1/condiments', function(req, res) {
	// Always return 200 OK
	res.status(200);

	//breakout request body into local vars
	var id = req.body.id;
	var available = req.body.available; 

	//see if this id already exists in Redis
	var exists = false;

	client.get(id, function(err, reply) {
		res.send('Redis reply: ' + reply.toString());
		if (id == reply.toString()) {
			exists = true;
		}

	});

 	res.send('DONE');
});

var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  console.log("Listening on " + port);
});